//#full-example
package com.example


import akka.actor.typed.ActorRef
import akka.actor.typed.ActorSystem
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

// сообщение, которое передается между акторами
// внутри number - Int, номер сообщения
// actors - массив из наших четырех акторов, что б адресовать
case class Pong(number: Int, actors: Array[ActorRef[Pong]])

// акторы все скопированы, код одинаковый - меняется только то, в какой актор пересылается сообщение
object Actor1 {
    // с помощью apply инициализируется поведение актора
    // Behavior[Pong] - тип возвращаемого значения, Pong это сообщение, Behavior - это поведение актора
    def apply(): Behavior[Pong] = Behaviors.receive { (context, message) => //receive - функция, которое описывает поведение при приеме сообщения
      // context - текущий контекст, можно использовать для логов. message - сообщение, на объект Pong
      context.log.info("actor 1 receive " + message.number + " message") // логирование номера сообщения
      Thread.sleep(1000) // подождать секунду (1000 мс)
      message.actors(3) ! Pong(message.number + 1, message.actors) // через ! отправляется сообщение следующему актору
      // 1 актор шлет четвертому. Так как в массиве нумерация с нуля, то (3) это четвертый
      // номер сообщения увеличивается на 1, массив с акторами также передается (message.actors)
      Behaviors.same // возвращаемое значение, говорит о том, чтобы не менять поведение этого актора на следующем сообщении
    }
}

object Actor2 {
    def apply(): Behavior[Pong] = Behaviors.receive { (context, message) =>
      context.log.info("actor 2 receive " + message.number + " message")
      Thread.sleep(1000)
      message.actors(2) ! Pong(message.number + 1, message.actors)
      Behaviors.same
    }
}

object Actor3 {
    def apply(): Behavior[Pong] = Behaviors.receive { (context, message) =>
      context.log.info("actor 3 receive " + message.number + " message")
      Thread.sleep(1000)
      message.actors(0) ! Pong(message.number + 1, message.actors)
      Behaviors.same
    }
}

object Actor4 {
    def apply(): Behavior[Pong] = Behaviors.receive { (context, message) =>
      context.log.info("actor 4 receive " + message.number + " message")
      Thread.sleep(1000)
      message.actors(1) ! Pong(message.number + 1, message.actors)
      Behaviors.same
    }
}

// инициализация
object ActorMain {
  case class Ping()

  def apply(): Behavior[Ping] = Behaviors.setup { context =>
    // создаем акторы
    val actor1 = context.spawn(Actor1(), "actor1")
    val actor2 = context.spawn(Actor2(), "actor2")
    val actor3 = context.spawn(Actor3(), "actor3")
    val actor4 = context.spawn(Actor4(), "actor4")
    val actorsArray = Array(actor1, actor2, actor3, actor4) // собираем их в массив
    actor1 ! Pong(1, actorsArray) //шлем первое сообщение первому актору
    Behaviors.same
  }
}

// Главный класс
object AkkaQuickstart extends App {
  val actorMain = ActorSystem(ActorMain(), "AkkaQuickStart") //инициализация системы акторов
}